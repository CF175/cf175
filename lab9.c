#include<stdio.h>
void swap(int *p,int *q);
int main()
{
 int x,y;
 printf("enter the numbers\n");
 scanf("%d%d",&x,&y);
 printf("numbers before swapping:%d,%d\n",x,y);
 swap(&x,&y);
 return 0;
}
void swap(int *p,int *q)
{int temp;
temp=*p;
*p=*q;
*q=temp;
printf("numbers after swapping are:%d,%d\n",*p,*q);
}